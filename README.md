# FoundryVTT Pathfinder 1E 中文系统

本MOD为FVTT的Pathfinder 1E 系统加入了中文。

**使用说明：**

* 1.首先在安装界面MOD安装中，安装本MOD。
* （安装链接：https://gitlab.com/xtlcme/foundry-vtt-pf1e-chn/raw/master/pf1e_cn/module.json）
* 2.进入游戏世界（DND5E），在【设置-管理MOD】中勾选本MOD。
* 3.在【设置-语言】中选择【中文】。
* 4.重启游戏世界。

